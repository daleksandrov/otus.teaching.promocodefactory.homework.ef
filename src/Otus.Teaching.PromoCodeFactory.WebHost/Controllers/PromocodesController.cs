﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IPromoCodeRepository _promoCodeRepository;
        private readonly IPreferenceRepository _preferenceRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;

        public PromocodesController(IPromoCodeRepository promoCodeRepository, IPreferenceRepository preferenceRepository, ICustomerRepository customerRepository, IMapper mapper)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodeRepository.GetAllAsync();
         
            var response = _mapper.Map<IEnumerable<PromoCode>, IList<PromoCodeShortResponse>>(promocodes);

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            var preference = await _preferenceRepository.GetFirstWhere(x => x.Name.Equals(request.Preference));
            PromoCode promoCode = new PromoCode()
            {
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,
                PreferenceId = preference.Id,
                BeginDate = DateTime.Now,
                EndDate = DateTime.MaxValue,
                EmployeeId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895")
            };

            await _customerRepository.SetToCustomersPreferenceAsync(promoCode);

            return Ok();
        }
    }
}