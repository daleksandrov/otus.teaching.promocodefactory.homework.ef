﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EFRepository<Customer>, ICustomerRepository
    {
        private readonly DataContext _dataContext;
        
        public CustomerRepository(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Customer> GetByIdWithPromoCodesAsync(Guid id)
        {
            return await _dataContext.Customers
                .Include(x => x.PromoCodes)
                .FirstOrDefaultAsync();
        }

        public async Task<Customer> GetByIdWithPreferencesAsync(Guid id)
        {
            return await _dataContext.Customers
                .Include(x => x.CustomerPreferences)
                .FirstOrDefaultAsync();
        }

        public async Task<Customer> UpdateAsync(Customer customer, IEnumerable<CustomerPreference> customerPreference)
        {
            if (customer == null)
            {
                throw new ArgumentNullException($"{nameof(UpdateAsync)} entity must not be null");
            }
            
            try{
                _dataContext.Set<CustomerPreference>().RemoveRange(customer.CustomerPreferences);
                
                foreach (var row in customerPreference)
                {
                    customer.CustomerPreferences.Add(row);
                }
                
                _dataContext.Update(customer);
                
                await _dataContext.SaveChangesAsync();
                
                return customer;
            }
            catch (Exception)
            {
                throw new Exception($"{nameof(customer)} could not be updated");
            }
        }
        
        public async Task SetToCustomersPreferenceAsync(PromoCode promoCode)
        {
            if (promoCode == null)
            {
                throw new ArgumentNullException($"{nameof(SetToCustomersPreferenceAsync)} promoCode must not be null");
            }

            try
            {


                var customers = _dataContext.Customers
                    .Where(x => x.CustomerPreferences.Any(y => y.PreferenceId == promoCode.PreferenceId));

                foreach (var customer in customers)
                {
                    PromoCode promoCodeRow = new PromoCode()
                    {
                        Id = Guid.NewGuid(),
                        Code = promoCode.Code,
                        PreferenceId = promoCode.PreferenceId,
                        BeginDate = promoCode.BeginDate,
                        EndDate = promoCode.EndDate,
                        PartnerName = promoCode.PartnerName,
                        ServiceInfo = promoCode.ServiceInfo,
                        CustomerId = customer.Id,
                        EmployeeId = promoCode.EmployeeId
                    };
                    await _dataContext.PromoCodes.AddAsync(promoCodeRow);
                }

                await _dataContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new Exception($"{nameof(promoCode)} could not be created", e);
            }
        }
    }
}