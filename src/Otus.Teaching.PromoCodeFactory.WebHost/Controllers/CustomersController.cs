﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IPreferenceRepository  _preferenceRepository;
        private readonly IMapper _mapper;

        public CustomersController(ICustomerRepository customerRepository, IPreferenceRepository preferenceRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = _mapper.Map<IEnumerable<Customer>, IList<CustomerShortResponse>>(customers);

            return Ok(response);
        }
        
        /// <summary>
        /// Получение клиента вместе с выданными ему промомкодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdWithPromoCodesAsync(id);

            var response = _mapper.Map<Customer, CustomerResponse>(customer);

            return Ok(response);
        }
        
        /// <summary>
        /// создание нового клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            var newId = Guid.NewGuid();

            var customerPreferences = new List<CustomerPreference>();
            customerPreferences.AddRange(preferences.Select(x =>
                new CustomerPreference()
                {
                    CustomerId = newId,
                    PreferenceId = x.Id
                }));

            var newCustomer = new Customer()
            {
                Id = newId,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                CustomerPreferences = customerPreferences
            };

            await _customerRepository.AddAsync(newCustomer);

            return Ok();
        }
        
        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var newPreferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            var customerPreferences = new List<CustomerPreference>();
            customerPreferences.AddRange(newPreferences.Select(x =>
                new CustomerPreference()
                {
                    CustomerId = id,
                    PreferenceId = x.Id
                }));
            
            var customer = await _customerRepository.GetByIdWithPreferencesAsync(id);
            await _customerRepository.UpdateAsync(customer, customerPreferences);

            return Ok();
        }
        
        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO Протестировать удаление
            var customer = await _customerRepository.GetByIdAsync(id);
            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}