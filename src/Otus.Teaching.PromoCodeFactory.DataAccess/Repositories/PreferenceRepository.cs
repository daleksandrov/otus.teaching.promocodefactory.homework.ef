﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PreferenceRepository : EFRepository<Preference>, IPreferenceRepository
    {
        private readonly DataContext _dataContext;
        
        public PreferenceRepository(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<Preference>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var preferences = await _dataContext.Preferences.Where(x => ids.Contains(x.Id)).ToListAsync();
            return preferences;
        }
    }
}