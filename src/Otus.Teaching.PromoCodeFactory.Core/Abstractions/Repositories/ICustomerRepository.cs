﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<Customer> GetByIdWithPromoCodesAsync(Guid id);
        Task<Customer> GetByIdWithPreferencesAsync(Guid id);
        Task<Customer> UpdateAsync(Customer customer, IEnumerable<CustomerPreference> preferences);
        Task SetToCustomersPreferenceAsync(PromoCode promoCode);
    }
}